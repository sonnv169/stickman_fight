﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cyforce.Stickman;

public class ConsData 
{
   
}

public class STRINGNAME
{
    public const string TagGround = "GROUND";
    public const string TagRebound = "REBOUND";
    public const string LayerPlayer = "Player";
    public const string LayerEnemy = "ENEMY";
}

[System.Serializable]
public enum TYPEBODY
{
    HEAD,
    BODY,
    LEG_UPPER,
    LEG_LOWER,
    ARM_UPPER,
    ARM_LOWER,
    WEAPON
}

public class CACULATION_DAME
{
    public float Dame;

    public CACULATION_DAME(PhysicPlayer physicPlayer, float dame,float dameBonus = 1f)
    {
        switch (physicPlayer.TypeBody)
        {
            case TYPEBODY.HEAD:
                Dame = dame*dameBonus;
                break;
            case TYPEBODY.BODY:
                Dame = 0f * dameBonus;
                break;
            case TYPEBODY.LEG_UPPER:
                Dame = 0f * dameBonus;
                break;
            case TYPEBODY.LEG_LOWER:
                Dame = dame * 0.8f * dameBonus;
                break;
            case TYPEBODY.ARM_UPPER:
                Dame = 0f * dameBonus;
                break;
            case TYPEBODY.ARM_LOWER:
                Dame = dame * 0.8f * dameBonus;
                break;
            case TYPEBODY.WEAPON:
                Dame = dame +  dame * dameBonus/100f;
                break;
            default:
                break;
        }
    }
}

public class CREAT_WEAPON
{
    public CREAT_WEAPON(Weapon weaponPrefabs, Vector2 posSpawn,Transform parent = null,bool isPlayer = true,PlayerInfomation _player = null)
    {
        Weapon wp = GameObject.Instantiate(weaponPrefabs,posSpawn,Quaternion.identity);
        
        wp.gameObject.SetActive(false);
        if (isPlayer)
        {
            wp.gameObject.layer = LayerMask.NameToLayer(STRINGNAME.LayerPlayer);
        }
        else
        {
            wp.gameObject.layer = LayerMask.NameToLayer(STRINGNAME.LayerEnemy);
        }
        if (parent != null)
        {
            wp.transform.SetParent(parent);
            wp.transform.localPosition = Vector3.zero;
        }
        if (_player == null)
        {
            Debug.LogWarning("Cant find Player infomation");
        }
        else
        {
            wp.GetComponent<PhysicPlayer>().PlayerInfomation = _player;
        }
        wp.gameObject.SetActive(true);
    }
}

public class CREAT_BULLET
{
    public CREAT_BULLET(BulletControll bulletPrefab,Transform posSpawn,Quaternion direction,Vector2 dir)
    {
       BulletControll wp = GameObject.Instantiate(bulletPrefab, posSpawn.position, Quaternion.identity);
        wp.transform.localRotation = direction;
        wp.direcTion = dir;
    }
}


[System.Serializable]
public class CAMERALIMIT
{
    public float minLimitX;
    public float maxLimitX;
    public float minLimitY;
    public float maxLimitY;
}

[System.Serializable]
public class PLAYER_SKIN
{
    public string name_Skin;
    public Sprite head;
    public Sprite body_Up;
    public Sprite body_Lower;
    public Sprite arm_Left_Upper;
    public Sprite arm_Right_Upper;
    public Sprite arm_Left_Lower;
    public Sprite arm_Right_Lower;
    public Sprite Leg_Left_Upper;
    public Sprite Leg_Right_Upper;
    public Sprite Leg_Left_Lower;
    public Sprite Leg_Right_Lower;
}

[System.Serializable]
public class PLAYER_SKIN_RENDERE
{
    public SpriteRenderer head;
    public SpriteRenderer body_Up;
    public SpriteRenderer body_Lower;
    public SpriteRenderer arm_Left_Upper;
    public SpriteRenderer arm_Right_Upper;
    public SpriteRenderer arm_Left_Lower;
    public SpriteRenderer arm_Right_Lower;
    public SpriteRenderer Leg_Left_Upper;
    public SpriteRenderer Leg_Right_Upper;
    public SpriteRenderer Leg_Left_Lower;
    public SpriteRenderer Leg_Right_Lower;
}

public class WEAR_SKIN
{
    public WEAR_SKIN(PLAYER_SKIN_RENDERE skinSprite,PLAYER_SKIN skin)
    {
        skinSprite.head.sprite = skin.head;
        skinSprite.body_Up.sprite = skin.body_Up;
        skinSprite.body_Lower.sprite = skin.body_Lower;
        skinSprite.arm_Left_Upper.sprite = skin.arm_Left_Upper;
        skinSprite.arm_Left_Lower.sprite = skin.arm_Left_Lower;
        skinSprite.arm_Right_Upper.sprite = skin.arm_Right_Upper;
        skinSprite.arm_Right_Lower.sprite = skin.arm_Right_Lower;
        skinSprite.Leg_Left_Upper.sprite = skin.Leg_Left_Upper;
        skinSprite.Leg_Left_Lower.sprite = skin.Leg_Left_Lower;
        skinSprite.Leg_Right_Upper.sprite = skin.Leg_Right_Upper;
        skinSprite.Leg_Right_Lower.sprite = skin.Leg_Right_Lower;
    }
}


[System.Serializable]
public class FIRST_CREAT_WEAPON
{
    public Vector3 localPositionPos;
    public Vector3 localRotation;
}

[System.Serializable]
public enum NAME_SKIN
{
    BAT_MAN,
    CAPITAN,
    GOKU,
    IRONMAN,
    MAIJBU
}

[System.Serializable]
public enum NAME_WEAPON
{
    BOOMERANG,
    KATANA,
    GUN,
    SHEILD

}