﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;

namespace Cyforce.Stickman
{
    public abstract class Weapon : MonoBehaviour
    {
        public float BonusDame;

        public float timeCoundown;

        public float rig2DMass;

        public Vector2 direction;

        public float rangeAttrack;

        public virtual void UseSkill()
        {

        }      

    }
}

