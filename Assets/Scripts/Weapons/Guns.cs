﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cyforce.Stickman;
using CnControls;

public class Guns : Weapon
{
    [SerializeField] private BulletControll bulletPrefabs;

    public override void UseSkill()
    {
        Vector2 dir = new Vector2(CnInputManager.GetAxis("Horizontal"), CnInputManager.GetAxis("Vertical"));
        CREAT_BULLET bullet = new CREAT_BULLET(bulletPrefabs, this.transform, this.transform.parent.localRotation,dir.normalized);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            UseSkill();
        }
    }
}
