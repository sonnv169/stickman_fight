﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cyforce.Stickman
{
    public class BalanceBody : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rb2D;

        [SerializeField] private float revSpeed = 50f;

        [SerializeField] private float stun;

        [SerializeField] private Vector2 jump;

        [SerializeField] private float rot;

        private void Start()
        {
            rb2D = GetComponent<Rigidbody2D>();
        }

        private void FixedUpdate()
        {
            ControllBalanceBody();
        }

        private void ControllBalanceBody()
        {
            if (stun < 1f)
            {
                rb2D.MoveRotation(rb2D.rotation + revSpeed * Time.fixedDeltaTime);
            }
            else
            {
                stun -= 1f;
                stun /= 1.1f;
            }
            Quaternion rotation = base.transform.rotation;
            rot = rotation.z;
            revSpeed = rot * -5000f;
        }
    }
}

