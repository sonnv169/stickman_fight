﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cyforce.Stickman
{
    public class BalancedLowerFoot : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rg;

        [SerializeField] private Rigidbody2D rgPied;

        [SerializeField] private Vector2 jump;

        [SerializeField] private bool haut;

        [SerializeField] private float puissanceJump;

        [SerializeField] private bool IsPlayer;

        [SerializeField] private PlayerDirection DirectionJoueur;

        [SerializeField] private int TimeControll;

        [SerializeField] private float PowerJump;

        private void Start()
        {
            rgPied = GetComponent<Rigidbody2D>();
        }

        private void FixedUpdate()
        {
            ControllBalanceLowerFoot();
        }

        private void ControllBalanceLowerFoot()
        {
            if (haut)
            {
                rg.MovePosition(rg.position + jump * Time.fixedDeltaTime);
            }
            TimeControll++;
            if (TimeControll > 100)
            {
                haut = false;
            }
        }

        private void OnCollisionEnter2D(Collision2D coll)
        {
            if (coll.transform.tag == STRINGNAME.TagGround)
            {
                TimeControll = 0;
                jump.y = PowerJump + puissanceJump;
                haut = true;
                DirectionJoueur.ColTime = 100;
            }
            if (coll.transform.tag == STRINGNAME.TagRebound)
            {
                TimeControll = 0;
                jump.y = 25f;
                haut = true;
                DirectionJoueur.ColTime = 100;
            }
        }

        private void OnCollisionExit2D(Collision2D coll)
        {
            haut = false;
            if (coll.transform.tag == STRINGNAME.TagGround)
            {
                DirectionJoueur.ColTime = 100;
            }
            if (coll.transform.tag == STRINGNAME.TagRebound)
            {
                DirectionJoueur.ColTime = 100;
            }
        }
    }
}

