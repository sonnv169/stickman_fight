﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDual : MonoBehaviour
{
    [SerializeField] private float offsetMin;

    [SerializeField] private float offsetMax;

    [SerializeField] private GameObject player;

    [SerializeField] private GameObject player2;

    [SerializeField] private float xpos;

    [SerializeField] private float ypos;

    [SerializeField] private float delay;

    [SerializeField] private Camera cam;

    [SerializeField] private Transform Enemy;

    [SerializeField] private float distance;

    [SerializeField] private CAMERALIMIT cameraLimit;

    void FixedUpdate()
    {
        controllCameraFollow();
    }

    private void controllCameraFollow()
    {
        Vector3 dir = (Enemy.position - player.transform.position).normalized;
        distance = Vector3.Distance(Enemy.position, player.transform.position);
        Vector3 position1 = player.transform.position;
        float x3 = position1.x;
        Vector3 position2 = player2.transform.position;
        float num = (x3 + position2.x) / 2f;
        Vector3 position3 = base.transform.position;
        xpos = (num - position3.x + delay + dir.x ) * 10f;
        Vector3 position4 = player.transform.position;
        float y3 = position4.y;
        Vector3 position5 = player2.transform.position;
        float num2 = (y3 + position5.y) / 2f;
        Vector3 position6 = base.transform.position;
        ypos = (num2 - position6.y + dir.y) * 3f;
        if (distance < offsetMin)
        {
            distance = offsetMin;
        }
        if (distance > offsetMax)
        {
            distance = offsetMax;
        }
        
        base.transform.Translate((Vector2.right  * (xpos + dir.x*distance/2f)) * Time.deltaTime);
        base.transform.Translate((Vector2.up  * (ypos + dir.y * distance / 2f)) * Time.deltaTime);
        if (base.transform.position.x <= cameraLimit.minLimitX)
        {
            base.transform.position = new Vector3(cameraLimit.minLimitX, base.transform.position.y, -10f);
        }
        if (base.transform.position.x >= cameraLimit.maxLimitX)
        {
            base.transform.position = new Vector3(cameraLimit.maxLimitX, base.transform.position.y, -10f);
        }
        if (base.transform.position.y <= cameraLimit.minLimitY)
        {
            base.transform.position = new Vector3(base.transform.position.x, cameraLimit.minLimitY, -10f);
        }
        if (base.transform.position.y >= cameraLimit.maxLimitY)
        {
            base.transform.position = new Vector3(base.transform.position.x, cameraLimit.maxLimitY, -10f);
        }
    }
}
