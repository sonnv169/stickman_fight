﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Vector2 limitCameraPosX;

    [SerializeField] private Vector2 limitCameraPosY;

    [SerializeField] private Vector2 offsetCamera;

    [SerializeField] private Transform target;

    [SerializeField] private Transform _PosEnemy;

    [SerializeField] private float MaxRange;

    private void OnEnable()
    {
        //EventManager.setCameraFollow += ActionCameraFollow;
    }

    private void OnDisable()
    {
       // EventManager.setCameraFollow -= ActionCameraFollow;
    }


    private void FixedUpdate()
    {
        Vector2 dir = (target.position - transform.position).normalized;
        Vector2 direction = (Vector2)target.position + offsetCamera * dir;
        if (Vector2.Distance((Vector2)transform.position, (Vector2)target.position) <= MaxRange - 0.2f
            || Vector2.Distance((Vector2)transform.position, (Vector2)target.position) >= MaxRange + 0.2f)
        {            
            Vector2 velocity = Vector2.Lerp(transform.position, direction, 20f * Time.deltaTime);
            //this.transform.Translate( new Vector3(velocity.x, velocity.y, -10f));
             this.transform.position = velocity;
        }

    }
    //private void ActionCameraFollow( Vector2 _PosEnemy)
    //{

    //    if (target == null)
    //    {
    //        return;           
    //    }
    //    Debug.Log("Camera " + Vector2.Distance(target.position,this.transform.position ));
    //    dir = (_PosEnemy - (Vector2)target.position).normalized;
    //    if (Vector2.Distance(this.transform.position,target.position) < rangeCamera - 0.1f || Vector2.Distance(this.transform.position, target.position) > rangeCamera + 0.1f)
    //    {

    //        Vector2 pos = Vector2.Lerp(transform.position, (Vector2)target.position + dir * rangeCamera, 10f * Time.fixedDeltaTime);
    //        this.transform.position = new Vector3(pos.x,pos.y,-10f);
    //    }        
    //}
}
