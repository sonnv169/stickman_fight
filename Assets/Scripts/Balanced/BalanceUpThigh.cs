﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cyforce.Stickman
{
    public class BalanceUpThigh : MonoBehaviour
    {
        [SerializeField] private GameObject OppositeThigh;

        [SerializeField] private Rigidbody2D rb;      

        [SerializeField] private float anglejambe;

        [SerializeField] private float direction;       

        [SerializeField] private float POWER;

        [SerializeField] private int state;

        [SerializeField] private int time;

        [SerializeField] private GameObject DirectionArm;      

        [SerializeField] private float revSpeed;

        [SerializeField] private bool rightHanded;

        [SerializeField] private GameObject LowerFoot;

        [SerializeField] private Vector2 WeaponMoove;

        private BalanceUpThigh autrejambe;

        private HingeJoint2D component;

        private HingeJoint2D component2;

        private JointAngleLimits2D limits;

        private PlayerDirection ArmMove;

        private void Start()
        {
            SetUpThigh();
        }

        private void FixedUpdate()
        {
            ControllBalanceThigh();
        }

        private void SetUpThigh()
        {
            component = LowerFoot.GetComponent<HingeJoint2D>();
            component2 = base.gameObject.GetComponent<HingeJoint2D>();
            limits = component.limits;
            ArmMove = DirectionArm.GetComponent<PlayerDirection>();
            autrejambe = OppositeThigh.GetComponent<BalanceUpThigh>();
        }

        private void ControllBalanceThigh()
        {
            anglejambe = component2.jointAngle;
            rb.MoveRotation(rb.rotation + revSpeed * direction * Time.fixedDeltaTime);
            if (ArmMove.direction.x > 0.1f || WeaponMoove.x > 0.1f)
            {
                limits.min = 0f;
                limits.max = 90f;
                component.limits = limits;
            }
            else if (ArmMove.direction.x < -0.1f || WeaponMoove.x < 0.1f)
            {
                limits.min = -90f;
                limits.max = 0f;
                component.limits = limits;
            }
            if (ArmMove.direction.x == 0f && WeaponMoove.x == 0f)
            {
                if (rightHanded)
                {
                    if (state == 1)
                    {
                        limits.min = -40f;
                        limits.max = 0f;
                        component.limits = limits;
                    }
                    if (state == 2)
                    {
                        limits.min = 0f;
                        limits.max = 40f;
                        component.limits = limits;
                    }
                }
                if (!rightHanded)
                {
                    if (state == 1)
                    {
                        limits.min = -40f;
                        limits.max = 0f;
                        component.limits = limits;
                    }
                    if (state == 2)
                    {
                        limits.min = 0f;
                        limits.max = 40f;
                        component.limits = limits;
                    }
                }
            }
            switch (state)
            {
                case 1:
                    direction = -1f;
                    time++;
                    if (time > 2 && autrejambe.state == state)
                    {
                        if (component2.jointAngle > anglejambe)
                        {
                            state = 2;
                            autrejambe.state = 1;
                        }
                        else
                        {
                            state = 1;
                            autrejambe.state = 2;
                        }
                    }
                    if ((Mathf.Abs(ArmMove.direction.x) > 0.4f || Mathf.Abs(WeaponMoove.x) > 0.4f) && component2.jointAngle > 45f && autrejambe.anglejambe < -45f)
                    {
                        state = 2;
                        time = 0;
                    }
                    break;
                case 2:
                    direction = 1f;
                    time++;
                    if (time > 2 && autrejambe.state == state)
                    {
                        if (component2.jointAngle < anglejambe)
                        {
                            state = 1;
                            autrejambe.state = 2;
                        }
                        else
                        {
                            state = 2;
                            autrejambe.state = 1;
                        }
                    }
                    if ((Mathf.Abs(ArmMove.direction.x) > 0.4f || Mathf.Abs(WeaponMoove.x) > 0.4f) && component2.jointAngle < -45f && autrejambe.anglejambe > 45f)
                    {
                        state = 1;
                        time = 0;
                    }
                    break;
            }
        }
    }
}

