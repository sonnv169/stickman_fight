using UnityEngine;
using CnControls;

namespace Cyforce.Stickman
{
    public class PlayerDirection : MonoBehaviour
    {

        public Vector2 direction;

        public float speed;

        public Rigidbody2D rb;

        public float Devx;

        public float Devy;

        public int PowerWeapon;

        public int Charge;

        public float stunBras;

        public float DirectionHaut;

        public int ColTime;       

        public void ControllPlayerMove(PlayerInfomation _player)
        {
            PowerWeapon--;
            stunBras /= 1.4f;
            Charge--;
            direction = new Vector2(CnInputManager.GetAxis("Horizontal"), CnInputManager.GetAxis("Vertical"));
            direction = new Vector2(direction.x + Devx, direction.y + Devy);
            direction = direction.normalized;
            _player.FlipHeadInDirection(direction.x);
            if (direction.y > 0f)
            {
                direction.y *= DirectionHaut;
            }
            if (ColTime > 0)
            {
                DirectionHaut = 1f;
                ColTime--;
            }
            else
            {
                DirectionHaut = 0f;
            }
            if (stunBras <= 1f)
            {
                rb.MovePosition(rb.position + direction * speed * Time.fixedDeltaTime);
            }
        }

       

        private void Action_Smile()
        {
            PowerWeapon = UnityEngine.Random.Range(0, 4);
            if (PowerWeapon != 2)
            {
                Devx = UnityEngine.Random.Range(-0.7f, 0.7f);
                Devy = UnityEngine.Random.Range(-0.7f, 0.7f);
            }
            if (PowerWeapon == 2)
            {
                Devx = 0f;
                Devy = 0f;
                direction = new Vector2(0f, 0f);
            }
        }
    }
}

