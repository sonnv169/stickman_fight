﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cyforce.Stickman
{
    public class PlayerInfomation : MonoBehaviour
    {
        [SerializeField] private float Heath;

        [SerializeField] private float attrack;

        [SerializeField] private PLAYER_SKIN_RENDERE playerSkinRender;

        [SerializeField] private WeaponManager weapons;

        [SerializeField] private bool isPlayer;

        [SerializeField] private SpriteRenderer rdhead;

        [SerializeField] private NAME_SKIN nameSkin;

        [SerializeField] private NAME_WEAPON nameWeapon;

        [SerializeField] private bool isSkin;

        [SerializeField] private Transform parentWeapon;

        [SerializeField] private PlayerDirection playerDirection;

        [SerializeField] private Transform Arm_lower;

        [SerializeField] private Transform weaponPos;

        public bool IsPlayer
        {
            get { return isPlayer; }
        }

        public float Attrack
        {
            get { return attrack; }
        }

        private bool isDirectionLeft = false;      


        private void Start()
        {
            if (!isSkin)
            {
                return;
            }
            if (!isPlayer)
            {
                rdhead.flipX = true;
            }
            WEAR_SKIN wear = new WEAR_SKIN(playerSkinRender, Skin.Instance.Player_Skin[(int)nameSkin]);
            CREAT_WEAPON wp = new CREAT_WEAPON(weapons.WeaponPrefabs[(int)nameWeapon],Vector2.zero,parentWeapon,isPlayer,this);
        }

        private void FixedUpdate()
        {
            if (isPlayer)
            {
                playerDirection.ControllPlayerMove(this);
            }
            
        }

        public void CaculationDame(float dame)
        {
            Heath -= dame;
        }

        public void FlipHeadInDirection(float xD)
        {
            
            if (isPlayer)
            {
                if (xD < 0 && !isDirectionLeft)
                {
                    rdhead.flipX = true;
                    isDirectionLeft = true;
                }
                if (xD > 0 && isDirectionLeft)
                {
                    rdhead.flipX = false;
                    isDirectionLeft = false;
                }
            }
           
        }

        public Vector2 Direction()
        {
            return (weaponPos.position - Arm_lower.position).normalized;
        }
    }
}

