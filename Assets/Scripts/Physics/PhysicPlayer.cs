﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cyforce.Stickman
{
    public class PhysicPlayer : MonoBehaviour, IDame
    {
        [SerializeField] private TYPEBODY typeBody;

        [SerializeField] private PlayerInfomation playerInfomation;

        [SerializeField] private Weapon bonusDameWeapon;

        public Vector2 dir
        {
            get { return playerInfomation.Direction(); }
        }

        public float BonusDameWeapon
        {
            get { return bonusDameWeapon.BonusDame; }
        }

        public TYPEBODY TypeBody
        {
            get { return typeBody; }
        }

        public PlayerInfomation PlayerInfomation
        {
            set { playerInfomation = value; }
        }

        public float PlayerInfomationAttrack
        {
            get { return playerInfomation.Attrack; }
        }

        public void Dame(PhysicPlayer physicPlayer, float _dame)
        {
            if (typeBody == TYPEBODY.ARM_LOWER || typeBody == TYPEBODY.LEG_LOWER || typeBody == TYPEBODY.WEAPON)
            {
                return;
            }
            float dame = 0f;
            if (physicPlayer.TypeBody == TYPEBODY.WEAPON && physicPlayer.playerInfomation != null)
            {

                CACULATION_DAME cDame = new CACULATION_DAME(physicPlayer, _dame, 50);
                dame = cDame.Dame;
            }
            else
            {
                CACULATION_DAME cDame = new CACULATION_DAME(physicPlayer, _dame, 1f);
                dame = cDame.Dame;
            }
            playerInfomation.CaculationDame(dame);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {           
            if (GetComponent<IDame>() != null && collision.collider.GetComponent<IDame>() != null)
            {
                PhysicPlayer physicPlayerCollison = collision.collider.GetComponent<PhysicPlayer>();
                IDame _dame = GetComponent<IDame>();
                 _dame.Dame(physicPlayerCollison, physicPlayerCollison.PlayerInfomationAttrack);
            }
        }       
    }
}

