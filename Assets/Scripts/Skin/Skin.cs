﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skin : MonoBehaviour
{
    public static Skin Instance;

    private void Awake()
    {
        Instance = this;
    }

    [SerializeField] private List<PLAYER_SKIN> playerSkin;

    public List<PLAYER_SKIN> Player_Skin
    {
        get { return playerSkin; }
    }
}
