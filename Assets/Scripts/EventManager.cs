﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventManager 
{
    public delegate void cameraFollow(Vector2 PosEnemy);

    public static event cameraFollow setCameraFollow;

    public static void DoCameraFollow(Vector2 _PosEnemy)
    {
        if (setCameraFollow != null)
        {
            setCameraFollow(_PosEnemy);
        }
    }

    public delegate void caculationDame(float dame);
    public static event caculationDame SetCaculationDame;
    public static void DoCaculationDame(float _dame)
    {
        if (SetCaculationDame != null)
        {
            SetCaculationDame(_dame);
        }
    }
}
